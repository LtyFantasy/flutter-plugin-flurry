import 'dart:async';

import 'package:flutter/services.dart';

class Flurry {
  static const MethodChannel _channel = const MethodChannel('flurry');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<Null> initialize(
      {String androidKey = "",
      String iosKey = "",
      bool enableLog = true}) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("api_key_android", () => androidKey);
    args.putIfAbsent("api_key_ios", () => iosKey);
    args.putIfAbsent("is_log_enabled", () => enableLog);

    await _channel.invokeMethod('initialize', args);

    return null;
  }

  static Future<void> setUserId(String userId) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("userId", () => userId);

    await _channel.invokeMethod('userId', args);
  }

  static Future<void> logEvent(String event,
      [Map<String, dynamic>? params]) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("event", () => event);
    args.putIfAbsent("params", () => params);
    await _channel.invokeMethod('logEvent', args);
  }

  static Future<void> logEventTimed(String event,
      [Map<String, dynamic>? params]) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("event", () => event);
    args.putIfAbsent("params", () => params);
    await _channel.invokeMethod('logEventTimed', args);
  }

  static Future<void> endTimedEvent(String event,
      [Map<String, dynamic>? params]) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("event", () => event);
    args.putIfAbsent("params", () => params);
    await _channel.invokeMethod('endTimedEvent', args);
    return null;
  }

  /// Sets and replaces (if any exist) the values for a property.
  static Future<void> setValues(String key, List<String> values) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    args.putIfAbsent("values", () => values);
    await _channel.invokeMethod('setValues', args);
  }

  /// Sets and replaces (if any exist) the single value for a property.
  static Future<void> setValue(String key, String value) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    args.putIfAbsent("value", () => value);
    await _channel.invokeMethod('setValue', args);
  }

  /// Adds the values to the property.
  static Future<void> addValues(String key, List<String> values) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    args.putIfAbsent("values", () => values);
    await _channel.invokeMethod('addValues', args);
  }

  /// Adds the single value to the property.
  static Future<void> addValue(String key, String value) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    args.putIfAbsent("value", () => value);
    await _channel.invokeMethod('addValue', args);
  }

  /// Removes the values from the property.
  static Future<void> removeValues(String key, List<String> values) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    args.putIfAbsent("values", () => values);
    await _channel.invokeMethod('removeValues', args);
  }

  /// Removes the single value from the property.
  static Future<void> removeValue(String key, String value) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    args.putIfAbsent("value", () => value);
    await _channel.invokeMethod('removeValue', args);
  }

  /// Removes all values from the property.
  static Future<void> removeKey(String key) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    await _channel.invokeMethod('removeKey', args);
  }

  /// Flag the value "true" to the property.
  static Future<void> flag(String key) async {
    Map<String, dynamic> args = <String, dynamic>{};
    args.putIfAbsent("key", () => key);
    await _channel.invokeMethod('flag', args);
  }
}
