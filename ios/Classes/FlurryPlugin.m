#import "FlurryPlugin.h"
#import <Flurry_iOS_SDK/Flurry_iOS_SDK.h>
@implementation FlurryPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"flurry"
                                     binaryMessenger:[registrar messenger]];
    FlurryPlugin* instance = [[FlurryPlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([@"initialize" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *apiKey = (NSString*)arguments[@"api_key_ios"];
        BOOL isLogEnabled = (BOOL)arguments[@"is_log_enabled"];
        
        NSString *appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        FlurryLogLevel level = isLogEnabled ? FlurryLogLevelAll : FlurryLogLevelNone;
        FlurrySessionBuilder* builder = [[[[FlurrySessionBuilder new]
                                           withLogLevel:level]
                                          withCrashReporting:YES]
                                         withAppVersion:appVersion];
        [Flurry startSession:apiKey withSessionBuilder:builder];
        
        result(nil);
    } else if ([@"logEvent" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *event = (NSString*)arguments[@"event"];
        NSDictionary *params = (NSDictionary *)arguments[@"params"];
        if([params isKindOfClass:[NSNull class]]) params = nil;
        FlurryEventRecordStatus status = [Flurry logEvent:event withParameters:params];
        NSLog(@"%d", status);
        result(nil);
    }  else if ([@"logEventTimed" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *event = (NSString*)arguments[@"event"];
        NSDictionary *params = (NSDictionary *)arguments[@"params"];
        if([params isKindOfClass:[NSNull class]]) params = nil;
        [Flurry logEvent:event withParameters:params timed:YES];
        result(nil);
    }  else if ([@"endTimedEvent" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *event = (NSString*)arguments[@"event"];
        NSDictionary *params = (NSDictionary *)arguments[@"params"];
        if([params isKindOfClass:[NSNull class]]) params = nil;
        [Flurry endTimedEvent:event withParameters:params];
        result(nil);
    } else if ([@"userId" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *userId = (NSString*)arguments[@"userId"];
        [Flurry setUserID:userId];
        result(nil);
    } else if ([@"setValues" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
        NSArray *values = (NSArray*)arguments[@"values"];
        [FlurryUserProperties set:key values:values];
        result(nil);
    }else if ([@"setValue" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
        NSString *value = (NSString*)arguments[@"value"];
        [FlurryUserProperties set:key value:value];
        result(nil);
    }else if ([@"addValues" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
        NSArray *values = (NSArray*)arguments[@"values"];
        [FlurryUserProperties add:key values:values];
        result(nil);
    }else if ([@"addValue" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
        NSString *value = (NSString*)arguments[@"value"];
        [FlurryUserProperties add:key value:value];
        result(nil);
    }else if ([@"removeValues" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
        NSArray *values = (NSArray*)arguments[@"values"];
        [FlurryUserProperties remove:key values:values];
        result(nil);
    } else if ([@"removeValue" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
        NSString *value = (NSString*)arguments[@"value"];
        [FlurryUserProperties remove:key value:value];
        result(nil);
    }else if ([@"removeKey" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *key = (NSString*)arguments[@"key"];
    
        [FlurryUserProperties remove:key];
        result(nil);
    } else if ([@"flag" isEqualToString:call.method]) {
        NSDictionary *arguments = (NSDictionary*)call.arguments;
        NSString *flag = (NSString*)arguments[@"key"];
        [FlurryUserProperties flag:flag];
        result(nil);
    }else {
        result(FlutterMethodNotImplemented);
    }
}

@end
