package mt.innovation.flurry;

import android.content.Context;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.flurry.android.FlurryAgentListener;

import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/**
 * FlurryPlugin
 */
public class FlurryPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {

    private Context activity;
    private MethodChannel channel;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        channel = new MethodChannel(binding.getBinaryMessenger(), "flurry");
        channel.setMethodCallHandler(this);
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
        channel.setMethodCallHandler(null);
    }

    private void logEvent(String message) {
        FlurryAgent.logEvent(message);
    }

    private void setUserId(String userId) {
        FlurryAgent.setUserId(userId);
    }

    @Override
    public void onMethodCall(MethodCall call, final Result result) {
        if (call.method.equals("initialize")) {
            String API_KEY = call.argument("api_key_android");
            boolean showLog = call.argument("is_log_enabled");

            // initializeFlurry(API_KEY, showLog);

            new FlurryAgent.Builder().withLogEnabled(showLog).withCaptureUncaughtExceptions(true)
                    .withContinueSessionMillis(10000).withLogLevel(Log.DEBUG).withListener(new FlurryAgentListener() {
                        @Override
                        public void onSessionStarted() {
                        }
                    }).build(activity, API_KEY);
            result.success(null);
        } else if (call.method.equals("logEvent")) {
            String message = call.argument("event").toString();
            try {
                HashMap params = call.argument("params");
                if (params != null) {
                    FlurryAgent.logEvent(message, params);
                } else {
                    logEvent(message);
                }
            } catch (Exception e) {
                logEvent(message);
            }
            result.success(null);

        } else if (call.method.equals("userId")) {
            String userId = call.argument("userId").toString();
            setUserId(userId);
            result.success(null);

        } else if (call.method.equals("logEventTimed")) {
            String message = call.argument("event").toString();
            try {
                HashMap params = call.argument("params");
                if (params != null) {
                    FlurryAgent.logEvent(message, params);
                } else {
                    logEvent(message);
                }
            } catch (Exception e) {
                logEvent(message);
            }
            result.success(null);
        } else if (call.method.equals("endTimedEvent")) {
            String message = call.argument("event").toString();
            FlurryAgent.endTimedEvent(message);
            result.success(null);
        } else if (call.method.equals("setValues")) {
            String key = call.argument("key").toString();
            List<String> values = call.argument("values");
            FlurryAgent.UserProperties.set(key, values);
            result.success(null);
        } else if (call.method.equals("setValue")) {
            String key = call.argument("key").toString();
            String value = call.argument("value");
            FlurryAgent.UserProperties.set(key, value);
            result.success(null);
        } else if (call.method.equals("addValues")) {
            String key = call.argument("key").toString();
            List<String> values = call.argument("values");
            FlurryAgent.UserProperties.add(key, values);
            result.success(null);
        } else if (call.method.equals("addValue")) {
            String key = call.argument("key").toString();
            String value = call.argument("value");
            FlurryAgent.UserProperties.add(key, value);
            result.success(null);
        } else if (call.method.equals("removeValues")) {
            String key = call.argument("key").toString();
            List<String> values = call.argument("values");
            FlurryAgent.UserProperties.remove(key, values);
            result.success(null);
        } else if (call.method.equals("removeValue")) {
            String key = call.argument("key").toString();
            String value = call.argument("value");
            FlurryAgent.UserProperties.remove(key, value);
            result.success(null);
        } else if (call.method.equals("removeKey")) {
            String key = call.argument("key").toString();
            FlurryAgent.UserProperties.remove(key);
            result.success(null);
        } else if (call.method.equals("flag")) {
            String key = call.argument("key").toString();
            FlurryAgent.UserProperties.flag(key);
            result.success(null);
        } else {
            result.notImplemented();
        }
    }

    @Override
    public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivityForConfigChanges() {
        activity = null;
    }

    @Override
    public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
        activity = binding.getActivity();
    }

    @Override
    public void onDetachedFromActivity() {
        activity = null;
    }
}
